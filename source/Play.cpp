/*
 *  Play.cpp
 *  Normal "play" state with images
 *
 *  Created by Marcelo Cohen on 05/12.
 *  Copyright 2012 PUCRS. All rights reserved.
 *
 */

#include <iostream>
#include <SDL.h>
#include "Graphics.h"
#include <cmath>
#include "CGame.h"
#include "Play.h"
#include "time.h"
#include "Shot.h"

using namespace std;

Play::Play(CGame* g)
{
    srand(time(NULL));

    squadBlue = new SquadBlue();
    squadBlue->build(g);

    squadRed = new SquadRed();
    squadRed->build(g);

    // Carrega a fonte Arial 28
    arial = new CFont();
    arial->loadFont("data/fonts/arialblack28.png", 448, 640);

    line_delimiter = new CImage();
    line_delimiter->loadImage("data/img/line.png");
    line_delimiter->setPosition(0,280);

    line_hud = new CImage();
    line_hud->loadImage("data/img/line.png");
    line_hud->setPosition(0,560);


    // Obtem ponteiro para audio engine
    audio = g->getAudioEngine();
    // Carrega som do arquivo
    if(audio)
        boomSoundSource= audio->addSoundSourceFromFile("data/audio/bomb.wav");

    keyState = SDL_GetKeyState(0); // get key state array
    cout << "Play Init Successful" << endl;
}

Play::~Play()
{
    delete squadBlue;
    delete squadRed;
    delete arial;
    delete line_delimiter;
    delete line_hud;

	cout << "Play destructor successful" << endl;
}

void Play::handleEvents(CGame* game)
{
	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
			case SDL_QUIT:
				game->quit();
				break;

            case SDL_KEYDOWN:
				switch(event.key.keysym.sym){
                    case SDLK_ESCAPE:
                        game->quit();
                        break;
                    default:
                        break;
                }
                break;
            case SDL_VIDEORESIZE:
                game->resize(event.resize.w, event.resize.h);
        }

        dirx = keyState[SDLK_RIGHT]*spd + keyState[SDLK_LEFT]*-spd;
        diry = keyState[SDLK_UP]*-spd + keyState[SDLK_DOWN]*spd;


    }
}

// Update e' chamado pelo loop principal de jogo
// getUpdateInterval retorna QUANTO tempo passou desde a ultima vez
// (tempo em ms)
void Play::update(CGame* game)
{

    for (unsigned int i = 0; i < squadRed->totalShot(); i++){
        squadRed->getShot(i)->update(game->getUpdateInterval());
    }

    for (unsigned int i = 0; i < squadBlue->totalShot(); i++){
        squadBlue->getShot(i)->update(game->getUpdateInterval());
    }


    // instaciação de cannon blue (time de cima) inclui tiros no vector de tiros
    Cannon *cannon = new Cannon('d');
    int totalblue = squadBlue->total();
    for (unsigned int i = 0; i < totalblue; i++){

        Ship *ship = squadBlue->get(i);

        ship->update(game->getUpdateInterval());
        ship->move();

        // Desenvolver solução para tipo do Cannon
        if (i > 0){
            int r = rand() % 500;
            if (r < 0.2){
                squadBlue->addShot(&cannon->makeShot(ship->getX(), ship->getY()));
            }
        }

        int totalshot = squadRed->totalShot();
        for (unsigned int j = 0; j < totalshot; j++){

            if(squadRed->getShot(j)->getY() < 0){
                squadRed->removeShot(j);
                j = 0;
                i = 0;
                totalshot = squadRed->totalShot();
            }
            else if(squadRed->getShot(j)->bboxCollision(squadBlue->get(i)) && squadRed->get(i)->getVisible())
            {
                squadBlue->get(i)->hitted();

                if (squadBlue->get(i)->getHitPoints() <= 0){
                    // Toca som
                    if(audio) audio->play2D(boomSoundSource);
                    squadBlue->get(i)->setVisible(false);
                    squadBlue->remove(i);
                }

                squadRed->getShot(j)->setVisible(false);

                cout << "i: " << i << endl;
                cout << "j: " << j << endl;

                squadRed->removeShot(j);

                j = 0;
                i = 0;
                totalshot = squadRed->totalShot();
                totalblue = squadBlue->total();

            }


        }

    }

    // instaciação de cannon red (time de baixa) inclui tiros no vector de tiros
    cannon = new Cannon('u');
    int totalred = squadRed->total();
    for (unsigned int i = 0; i < totalred; i++){
        squadRed->get(i)->update(game->getUpdateInterval());
        squadRed->get(i)->move();

        if (i > 0){
            int r = rand() % 500;
            if (r < 0.2){
                squadRed->addShot(&cannon->makeShot(squadRed->get(i)->getX(), squadRed->get(i)->getY()));
            }
        }

        int totalshot = squadBlue->totalShot();
        for (unsigned int j = 0; j < totalshot; j++){

            if((squadBlue->getShot(j)->getY()) > 600){
                squadBlue->removeShot(j);
                j = 0;
                i = 0;
                totalshot = squadBlue->totalShot();
                totalred = squadRed->total();
            }
            else if(squadBlue->getShot(j)->bboxCollision(squadRed->get(i)) && squadRed->get(i)->getVisible())
            {
                cout << "Boom!" << endl;

                squadRed->get(i)->hitted();

                if (squadRed->get(i)->getHitPoints() <= 0){
                    // Toca som
                    if(audio) audio->play2D(boomSoundSource);
                    squadRed->get(i)->setVisible(false);
                    squadRed->remove(i);
                }

                squadBlue->getShot(j)->setVisible(false);

                cout << "i: " << i << endl;
                cout << "j: " << j << endl;

                squadBlue->removeShot(j);

                j = 0;
                i = 0;
                totalshot = squadBlue->totalShot();
                totalred = squadRed->total();
            }
        }

    }




}

void Play::draw(CGame* game)
{
    glClearColor(0.0,0.1,0.5,1); // fundo da tela: cinza escuro
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    int w = game->getScreen()->w;
    int h = game->getScreen()->h;

    // Desenha a linha de divisão central e do hud
    line_delimiter->draw();
    line_hud->draw();

    // desenha barcos azuis
    for (unsigned int i = 0; i < squadBlue->total(); i++){
        squadBlue->get(i)->draw();
    }
    // desenha tiros azuis
    for (unsigned int i = 0; i < squadBlue->totalShot(); i++){
        squadBlue->getShot(i)->draw();
    }
    //desenha barcos vermelhos
    for (unsigned int i = 0; i < squadRed->total(); i++){
        squadRed->get(i)->draw();
    }
    //desenha tiros vermelhos
    for (unsigned int i = 0; i < squadRed->totalShot(); i++){
        squadRed->getShot(i)->draw();
    }


    // Desenho de texto grafico
    arial->draw(10, h - 40,"Batalha Naval");
    //arial->draw(game->getWidth() - 100, h - 40,"Red: ");

    SDL_GL_SwapBuffers();
}

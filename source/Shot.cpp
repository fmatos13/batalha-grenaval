#include "Shot.h"


Shot::Shot(int x, int y, char direction)
{

    this->loadImage("data/img/Char14.png");
    this->setPosition(x,y);

    int shotSpeed = 30;

    if (direction == 'u')
        setYspeed(-shotSpeed);

    if (direction == 'd')
    {
        setYspeed(shotSpeed);
        setRotation(180);
    }

}

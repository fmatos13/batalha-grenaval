#include "Carrier.h"

Carrier::Carrier()
{
    this->hitpoints = 8;
    this->direction = 'r';
    this->loadImage("data/img/carrier.png");
}

void Carrier::move()
{
    if (direction == 'r' && x >= maxRangeX){
        setXspeed(-(getXspeed()));
        direction = 'l';
        setRotation(180);
    }

    if (direction == 'l' && x <= minRangeX){
        setXspeed(-(getXspeed()));
        direction = 'r';
        setRotation(0);
    }

}

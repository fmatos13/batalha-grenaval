// Class SquadRed
#include "SquadRed.h"

void SquadRed::buildCarrier(CGame* g)
{
    // Carrier
    Carrier *carrier = new Carrier();
    carrier->setPosition(g->getWidth()-carrier->getWidth() -60, 550 - carrier->getHeight());
    carrier->setRangeX(0, g->getWidth() - carrier->getWidth());
    carrier->setXspeed(-50);
    carrier->setDirection('l');
    carrier->setRotation(180);
    carrier->setYspeed(0);
    this->add(carrier);
}

void SquadRed::buildCruiser(CGame* g)
{
     int range = g->getWidth()/3;
    int rangeBegin = 0;
    for(int i=1; i <= 3; i++ )
    {
        Cannon *cannon = new Cannon('u');
        Cruiser *cruiser = new Cruiser(cannon);
        cruiser->setRangeX(rangeBegin, range * i - cruiser->getWidth());
        cruiser->setPosition(rangeBegin, 490 - cruiser->getHeight());

        rangeBegin = range * i + 1;

        cruiser->setXspeed(70);
        this->add(cruiser);
    }

    range = g->getWidth()/2;
    rangeBegin = 0;
    for(int i=1; i <= 2; i++){

        Cannon *cannon = new Cannon('u');
        Cruiser *cruiser = new Cruiser(cannon);
        cruiser->setRangeX(rangeBegin, range * i - cruiser->getWidth());
        cruiser->setPosition(rangeBegin, 460 - (cruiser->getHeight()*2));

        rangeBegin = range * i + 1;

        cruiser->setXspeed(40);
        this->add(cruiser);
    }
}


void SquadRed::buildSubmarine(CGame* g)
{
    int minRangeX = 10;
    int minRangeY = 290;
    int maxRangeY = minRangeY + 80;
    for(int i=1; i <= 4; i++)
    {
        Cannon *cannon = new Cannon('u');
        Submarine *submarine= new Submarine(cannon);

        // Position and Range
        submarine->setRangeX(minRangeX, g->getWidth()-submarine->getWidth());
        submarine->setRangeY(minRangeY, maxRangeY);
        submarine->setPosition((100 + submarine->getWidth()/2) * i, minRangeY);

        submarine->setXspeed(100);
        this->add(submarine);
    }

    for(int i=1; i <= 4; i++)
    {
        Cannon *cannon = new Cannon('u');
        Submarine *submarine= new Submarine(cannon);

        // Position and Range
        submarine->setRangeX(minRangeX, g->getWidth() - submarine->getWidth());
        submarine->setRangeY(minRangeY, maxRangeY);

        submarine->setPosition(g->getWidth()-submarine->getWidth() - (i * 100), maxRangeY);
        submarine->setDirection('l');
        submarine->setXspeed(-100);
        this->add(submarine);
    }
}

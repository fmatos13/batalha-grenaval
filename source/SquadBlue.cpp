// Class SquadBlue
#include "SquadBlue.h"

void SquadBlue::buildCarrier(CGame* g)
{
    // Carrier
    Carrier *carrier_blue = new Carrier();
    carrier_blue->setPosition(60, 10);
    carrier_blue->setRangeX(0, g->getWidth() - carrier_blue->getWidth());
    carrier_blue->setXspeed(50);
    carrier_blue->setYspeed(0);
    this->add(carrier_blue);
}

void SquadBlue::buildCruiser(CGame* g)
{
     int range = g->getWidth()/3;
    int rangeBegin = 0;
    for(int i=1; i <= 3; i++ )
    {
        Cannon *cannon = new Cannon('d');
        Cruiser *cruiser = new Cruiser(cannon);
        cruiser->setRangeX(rangeBegin, range * i - cruiser->getWidth());
        cruiser->setPosition(rangeBegin, 70);

        rangeBegin = range * i + 1;

        cruiser->setXspeed(70);
        this->add(cruiser);
    }

    range = g->getWidth()/2;
    rangeBegin = 0;
    for(int i=1; i <= 2; i++){
        Cannon *cannon = new Cannon('d');
        Cruiser *cruiser = new Cruiser(cannon);
        cruiser->setRangeX(rangeBegin, range * i - cruiser->getWidth());
        cruiser->setPosition(rangeBegin, 100 + cruiser->getHeight());

        rangeBegin = range * i + 1;

        cruiser->setXspeed(40);
        this->add(cruiser);
    }
}


void SquadBlue::buildSubmarine(CGame* g)
{
    int minRangeX = 10;
    int minRangeY = 160;
    int maxRangeY = g->getHeight()/2-60;
    for(int i=1; i <= 4; i++)
    {
        Cannon *cannon = new Cannon('d');
        Submarine *submarine= new Submarine(cannon);

        // Position and Range
        submarine->setRangeX(minRangeX, g->getWidth()-submarine->getWidth());
        submarine->setRangeY(minRangeY, maxRangeY);
        submarine->setPosition((100 + submarine->getWidth()/2) * i, minRangeY);

        submarine->setXspeed(100);
        this->add(submarine);
    }

    for(int i=1; i <= 4; i++)
    {
        Cannon *cannon = new Cannon('d');
        Submarine *submarine= new Submarine(cannon);

        // Position and Range
        submarine->setRangeX(minRangeX, g->getWidth() - submarine->getWidth());
        submarine->setRangeY(minRangeY, maxRangeY);

        submarine->setPosition(g->getWidth()-submarine->getWidth() - (i * 100), maxRangeY);
        submarine->setDirection('l');
        submarine->setXspeed(-100);
        this->add(submarine);
    }
}

#include "Cruiser.h"

Cruiser::Cruiser(Cannon *cannon)
{
    this->hitpoints = 3;
    this->direction = 'r';
    this->cannon = cannon;
    this->loadImage("data/img/cruiser.png");
}

void Cruiser::move()
{
    if (direction == 'r' && x >= maxRangeX){
        setXspeed(-(getXspeed()));
        direction = 'l';
        setRotation(180);
    }

    if (direction == 'l' && x <= minRangeX){
        setXspeed(-(getXspeed()));
        direction = 'r';
        setRotation(0);
    }
}

Shot& Cruiser::shot()
{
    return *( &cannon->makeShot(this->getX(), this->getY()) );
}

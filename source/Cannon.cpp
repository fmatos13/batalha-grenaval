#include "Cannon.h"

Cannon::Cannon(char direction)
{
    this->direction = direction;
}

Shot& Cannon::makeShot(int x, int y)
{
    return *(new Shot(x, y, this->direction));
}

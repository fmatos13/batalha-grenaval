#include <iostream>
#include "Submarine.h"
using namespace std;

Submarine::Submarine(Cannon *cannon)
{
    this->hitpoints = 1;
    this->direction = 'r'; // right direction
    this->cannon = cannon;
    this->loadImage("data/img/submarino.png");
}

void Submarine::move()
{
    if (direction == 'r' && x >= maxRangeX){
        setYspeed(getXspeed());
        setXspeed(0);
        direction = 'd'; // down
        setRotation(90);
    }
    else if (direction == 'l' && x <= minRangeX){

        setYspeed(getXspeed());
        setXspeed(0);
        direction = 'u'; // left
        setRotation(270);
    }
    else if (direction == 'u' && y <= minRangeY){

        setXspeed(-(getYspeed()));
        setYspeed(0);
        direction = 'r'; // right
        setRotation(0);
    }
    else if (direction == 'd'&& y >= maxRangeY){

        setXspeed(-getYspeed());
        setYspeed(0);
        direction = 'l'; // left
        setRotation(180);
    }

}

Shot& Submarine::shot()
{
    return *( &cannon->makeShot(this->getX(), this->getY()) );
}

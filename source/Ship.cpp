#include "Ship.h"

void Ship::hitted()
{
    if (hitpoints > 0){
        hitpoints--;
    }
}

int Ship::getHitPoints()
{
    return hitpoints;
}

void Ship::setRangeX(int begin, int end)
{
    minRangeX = begin;
    maxRangeX = end;
};

void Ship::setRangeY(int begin, int end)
{
    minRangeY = begin;
    maxRangeY = end;
}


int Ship::getMinRangeX()
{
    return minRangeX;
}

int Ship::getMaxRangeX()
{
    return maxRangeX;
}

int Ship::getMinRangeY()
{
    return minRangeY;
}

int Ship::getMaxRangeY()
{
    return maxRangeY;
}

void Ship::setDirection(char direction)
{
    this->direction = direction;
}

char Ship::getDirection()
{
    return direction;
}

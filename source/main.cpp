/*
 * Exemplo de uso do framework de jogo
 */

#include <iostream>
#include <cstdlib>
#include <string>
#include <cmath>

#include <SDL.h>
#include "CGame.h"
#include "Play.h"

int main(int argc, char **argv)
{
    CGame game(15,60);

    game.init("Game",800,600,0,false);

    Play playState(&game); // init play state

    game.changeState(&playState);

	while(game.isRunning())
	{
		game.handleEvents();
		game.update();
		game.draw();
	}

	// cleanup the engine
	game.clean();

    return 0;
}

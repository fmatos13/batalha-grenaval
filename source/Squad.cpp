// Class Squad
#include <iostream>
#include "Squad.h"
#include <stdexcept>
using namespace std;

Squad::~Squad()
{
    vet.clear();
    vetShot.clear();
}

void Squad::add(Ship* obj)
{
    vet.push_back(obj);
}


void Squad::remove(int pos)
{
    if (pos > 0 || pos < vet.size() - 1){
        //return NULL;
        try {
            vet.erase (vet.begin() + pos);
        }catch(out_of_range e){
            cout << "Out Of Range !!!!" << endl;
        }
    }
}

void Squad::build(CGame* g)
{
    buildCarrier(g);
    buildCruiser(g);
    buildSubmarine(g);
}

Ship* Squad::get(int pos)
{
    if (pos < 0 || pos > vet.size() - 1)
        return NULL;

    return vet[pos];
}

void Squad::addShot(Shot* obj)
{
    vetShot.push_back(obj);
}

Shot* Squad::getShot(int pos)
{
    if (pos < 0 || pos > vetShot.size() - 1)
        return NULL;

    return vetShot[pos];
}

void Squad::removeShot(int pos)
{
    if (pos > 0 || pos < vetShot.size() - 1){
        //return NULL;
        try {
            vetShot.erase (vetShot.begin() + pos);
        }catch(out_of_range e){
            cout << "Out Of Range !!!!" << endl;
        }
    }
}

#ifndef CANNON_H
#define CANNON_H

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "Shot.h"

using namespace std;

class Cannon
{
    public:
        Cannon(char direction);
        bool canShot();
        Shot& makeShot(int x, int y);

    private:
        char direction;
};


#endif

#ifndef SHIP_H
#define SHIP_H

#include <iostream>
#include "CImage.h"
#include "Cannon.h"
using namespace std;

class Ship : public CImage
{
    public:
        ~Ship(){};
        void hitted();
        int getHitPoints();
        virtual void move () = 0;
        void shot(); // return class Shot
        void setRangeX(int begin, int end);
        void setRangeY(int begin, int end);
        int getMinRangeX();
        int getMaxRangeX();
        int getMinRangeY();
        int getMaxRangeY();
        void setDirection(char direction);
        char getDirection();
        Cannon *cannon;

    protected:
        int hitpoints;
        char direction;
        int minRangeX, maxRangeX, minRangeY, maxRangeY;
        bool canShot();
};

#endif

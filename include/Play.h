/*
 *  Play.h
 *  Normal "play" state with images
 *
 *  Created by Marcelo Cohen on 05/12.
 *  Copyright 2011 PUCRS. All rights reserved.
 *
 */

#ifndef PLAY_H
#define PLAY_H

#include <SDL.h>
#include "CGameState.h"
#include "CImage.h"
#include "CFont.h"
#include "SquadBlue.h"
#include "SquadRed.h"


class Play : public CGameState
{
    public:

    Play(CGame* game);
    ~Play();

    void handleEvents(CGame* game);
    void update(CGame* game);
    void draw(CGame* game);

    private:

    float dirx, diry; // movement direction
    CFont* arial;     // the game font

    CImage *player;   // player image
    CImage *enemy;    // enemy image

    Shot *shot_red;
    Shot *shot_black;
    Cannon *cannon;
    SquadBlue *squadBlue;
    SquadRed *squadRed;

    CImage* line_delimiter; // line that separate the fields
    CImage* line_hud; // line that separate the fields

    Uint8* keyState;  // array to store keyboard state
    float spd;  // movement speed

    irrklang::ISoundEngine* audio;
    irrklang::ISoundSource* boomSoundSource;
};

#endif

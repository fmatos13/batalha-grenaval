#ifndef SQUADBLUE_H
#define SQUADBLUE_H

#include "Squad.h"

class SquadBlue: public Squad
{
    public:
        void buildCarrier(CGame* g);
        void buildCruiser(CGame* g);
        void buildSubmarine(CGame* g);
};

#endif

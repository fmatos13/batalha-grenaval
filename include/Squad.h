#ifndef SQUAD_H
#define SQUAD_H

#include <vector>
#include <Ship.h>
#include "CGameState.h"
#include "Carrier.h"
#include "Cruiser.h"
#include "Submarine.h"
#include "Shot.h"

class Squad {

    public:
        ~Squad();
        void add(Ship* obj);
        Ship* get(int pos);
        int total(){ return vet.size(); }
        void remove(int pos);
        void build(CGame* g);
        virtual void buildCarrier(CGame* g) = 0;
        virtual void buildCruiser(CGame* g) = 0;
        virtual void buildSubmarine(CGame* g) = 0;

        void addShot(Shot* obj);
        Shot* getShot(int pos);
        int totalShot(){ return vetShot.size(); }
        void removeShot(int pos);

    private:
        vector<Ship*> vet;
        vector<Shot*> vetShot;

};

#endif


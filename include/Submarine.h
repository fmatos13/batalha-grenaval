#ifndef SUBMARINE_H
#define SUBMARINE_H

#include "Ship.h"
#include "Cannon.h"

class Submarine: public Ship
{
    public:
        Submarine (Cannon *cannon);
        void move();
        Shot& shot();
        Cannon *cannon;
};

#endif

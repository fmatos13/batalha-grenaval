#ifndef CRUISER_H
#define CRUISER_H

#include "Ship.h"
#include "Cannon.h"

class Cruiser: public Ship
{
    public:
        Cruiser (Cannon *cannon);
        void move();
        Shot& shot();
        Cannon *cannon;
};

#endif

#ifndef SHOT_H
#define SHOT_H

#include <iostream>
#include "CImage.h"

using namespace std;


class Shot : public CImage
{
    public:
        Shot(int x, int y, char direction);

};

#endif


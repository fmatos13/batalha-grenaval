#ifndef SQUADRED_H
#define SQUADRED_H

#include "Squad.h"

class SquadRed: public Squad
{
    public:
        void buildCarrier(CGame* g);
        void buildCruiser(CGame* g);
        void buildSubmarine(CGame* g);
};

#endif


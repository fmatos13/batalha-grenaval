#ifndef CARRIER_H
#define CARRIER_H

#include <iostream>
#include "Ship.h"

class Carrier : public Ship
{
    public:
        Carrier();
        void move();
};

#endif

